<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CalController extends Controller
{

    /**
     * Show the ics for the given user.
     *
     * @param  String  $country
     * @param  String  $region
     * @param  String  $city
     * @return Response
     */
    public function show($country, $region, $city)
    {

        Log::debug('Parameters:'
                .', country: '.$country
                .', region: '.$region
                .', city: '.$city
            );

        $vCalendar = new Calendar('com.thespia.skycal');

        $baseURL = env('BASE_REMOTE_URL', 'http://base.url.not.set/properly/');

        $feed = \Feeds::make($baseURL.$country.'_'.$region.'_'.$city.'.xml');

        $items = $feed->get_items(); //grab all items inside the rss
        foreach($items as $item):

            // Create new blank iCal event
            $vEvent = new Event();

            // Create new empty collection for the details
            $details = collect([]);

            // The description of the RSS item contains rows of useful information
            // separated by '<br>'
            foreach(explode('<br>',$item->get_description()) as $keyvaluepairstring) {
                $keyvaluepairarray = explode(':',$keyvaluepairstring,2);

                if (trim($keyvaluepairarray[0])) {
                    $details->prepend(
                        trim(($keyvaluepairarray[1])),
                        trim(strtolower($keyvaluepairarray[0]))
                        );
                }
            } // next keyvaluepair


            // date = eg Saturday Aug 18, 2018
            // time = eg 6:16 AM
            $startDateTime = Carbon::parse($details['date'].' '.$details['time'], 'UTC');
            $vEvent->setDtStart($startDateTime);


            // Note that I'm assuming that the duration is always in minutes.
            // It is typically of the form '6 minutes' or '1 minute' or 'less than 1 minute'.
            // So I'm just taking the digit/s from the duration text.
            $duration = preg_replace("/[^0-9]/", '', $details['duration']);
            $endDateTime = $startDateTime->copy()->addMinutes($duration);
            $vEvent->setDtEnd($endDateTime);

            // We're jut going to give the ical in the user's local timezone
            $vEvent->setUseUtc(false); // ie use local time, no timezone specified

            // approach = eg 10° above WNW
            // departure = eg 10° above NNW
            // maximum elevation = eg 12°
            $vEvent->setSummary(
                'ISS Sighting'
                ."\n".$details['approach'].' to '.$details['departure']
                ."\nMax elevation ".$details['maximum elevation']
                );
            $vEvent->setDescriptionHTML(
                'ISS Sighting'
                ."\n<b>".$details['approach'].'</b> to <b>'.$details['departure'].'</b>'
                ."\nMax elevation <b>".$details['maximum elevation'].'</b>'
                );

            // Just use the name of the city for the location
            // One day I'll work out how to get the latitude / longituded in there too
            $vEvent->setLocation($city);
            // $vEvent->setLocation("Infinite Loop\nCupertino CA 95014",
            //         'Infinite Loop', '37.332095,-122.030743'); // for Apple devices

            $vCalendar->addComponent($vEvent);

        endforeach;



        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="cal.ics"');

        echo $vCalendar->render();

    }


}