# Cheat's way (very easy, a bit naughty)

## Bootstrap template
- Grab a simple one - perhaps the "Webpage" one from [w3schools](https://www.w3schools.com/bootstrap/bootstrap_templates.asp) 
or the [Sticky Footer](https://getbootstrap.com/docs/4.1/examples/sticky-footer/) one from getbootstrap.

## How to do the main visible page - new way

### Ask the user for their location
- Use standard [geolocation](https://www.w3schools.com/html/html5_geolocation.asp) API
- Use [about:config](https://security.stackexchange.com/questions/147166/how-can-you-fake-geolocation-in-firefox) to test other locations

### Get list of [SpotTheStation locations](https://spotthestation.nasa.gov/js/marker_list.js)

### Calculate the distance [from the user](https://www.geodatasource.com/developers/php) for each location

###Show the closest one (or two or three)
 
## How to do the main visible page - old way

### Scrape the list of countries from [this NASA page](https://spotthestation.nasa.gov/sightings/index.cfm)
- List is in select id="select01"
- Easiest way I've found so far is to use [Goutte](https://github.com/FriendsOfPHP/Goutte)

### Scrape the list of regions / states from the option list for that country 
- eg for Australia it's [here](https://spotthestation.nasa.gov/sightings/location_files/Australia.cfm), for the USA it's [here](https://spotthestation.nasa.gov/sightings/location_files/United_States.cfm)
- can sometimes give no regions, eg the [Bahamas](https://spotthestation.nasa.gov/sightings/location_files/Bahamas.cfm) page is <option value="Bahamas_None">None

### Scrape the list of cities from the city list for that country and region
- eg [here](https://spotthestation.nasa.gov/sightings/location_files/Bahamas_None.cfm) or [here](https://spotthestation.nasa.gov/sightings/location_files/Australia_Tasmania.cfm)
  
### Lastly, present the URL for the iCal calendar
- Should be pretty straightforward - build from Country / Region / City, eg myurl.com/ical/United_States/American_Samoa/National_Park_of_American_Samoa or myurl.com/ical/Australia/South_Australia/Adelaide
 
## How to build the iCal calendar

### Get the next few pass times for that given country / region / city
- Scrape them from the NASA page for that country / region / city, eg [the Adelaide page](https://spotthestation.nasa.gov/sightings/view.cfm?country=Australia&region=South_Australia&city=Adelaide)

### Build an iCal calendar 
- Use [markuspoerschke/iCal](https://github.com/markuspoerschke/iCal)

# New, simpler way

## How to do the main visible page

### Get the user's location [LAT & LONG]
- Get them to pick their town from the [Geobytes auto-complete list](http://www.geobytes.com/free-ajax-cities-jsonp-api/)
- Possibly default in a guesstimate based on IP number using the [Geobytes Get City Details API](http://www.geobytes.com/get-city-details-api/)
- Show them the location they've picked using the [Google Maps API](https://developers.google.com/maps/tutorials/fundamentals/adding-a-google-map)

### From the LAT & LONG, get their timezone
- Just use [Google's API](https://developers.google.com/maps/documentation/timezone/)
- Give them an option to override and select the timezone from [the PHP list](http://php.net/manual/en/timezones.php)?
  
### Lastly, present the URL for the iCal calendar
- Should be pretty straightforward - build from lat, long and tz
 
## How to build the iCal calendar

### For the given location, get the next few ISS pass times
 - Use [Open Notify's API](http://open-notify.org/Open-Notify-API/ISS-Pass-Times/)
 
### Build an iCal calendar 
- Use [markuspoerschke/iCal](https://github.com/markuspoerschke/iCal)

# Old, more complex way

## How to do the main visible page

### Get the user's location [LAT & LONG]
- First, guesstimate based on IP number using the [Geobytes Get City Details API](http://www.geobytes.com/get-city-details-api/)
- If they don't agree with that, ask to detect it using the [HTML5](http://code.tutsplus.com/tutorials/html5-apps-positioning-with-geolocation--mobile-456) [geolocation](http://www.w3schools.com/htmL/html5_geolocation.asp) [thingy](http://stackoverflow.com/questions/4908370/html5-geolocation-implementation)
- If that doesn't work either, get them to pick their town from the [Geobytes auto-complete list](http://www.geobytes.com/free-ajax-cities-jsonp-api/)
- As a last resort, allow them to enter the lat and long as numbers
- In any case, show them the location they've picked using the [Google Maps API](https://developers.google.com/maps/tutorials/fundamentals/adding-a-google-map)

### From the LAT & LONG, get their timezone
- Just use [Google's API](https://developers.google.com/maps/documentation/timezone/)
- Give them an option to override and select the timezone from [the PHP list](http://php.net/manual/en/timezones.php)?
  
### Lastly, present the URL for the iCal calendar
- Should be pretty straightforward - build from lat, long and tz
 
## How to build the iCal calendar

### For the given location, get the next few ISS pass times
 - Use [Open Notify's API](http://open-notify.org/Open-Notify-API/ISS-Pass-Times/)
 
### Build an iCal calendar 
- Use [markuspoerschke/iCal](https://github.com/markuspoerschke/iCal)