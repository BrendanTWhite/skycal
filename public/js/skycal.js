// skycal.js


var results = document.getElementById("results");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(buildURL, geoError);
    } else {
        results.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function geoError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            results.innerHTML = "User denied the request for Geolocation.<br/>"+error.message
            break;
        case error.POSITION_UNAVAILABLE:
            results.innerHTML = "No location information was obtained.<br/>"+error.message
            break;
        case error.TIMEOUT:
            results.innerHTML = "The request to get user location timed out.<br/>"+error.message
            break;
        case error.UNKNOWN_ERROR:
            results.innerHTML = "An unknown error occurred.<br/>"+error.message
            break;
    }
}

function buildURL(position) {

    console.log("Current position: Lat " + position.coords.latitude +
    ", Long: " + position.coords.longitude);

    // Pick a city (any city) to start with
    closestCitySoFar = addressPoints[1];
    shortestDistanceSoFar = distance(
			position.coords.latitude, position.coords.longitude,
			closestCitySoFar[1], closestCitySoFar[2]);
    console.log('Initial distance to beat: '+ shortestDistanceSoFar);

    $.each(addressPoints, function( key, thisCity ) {

    	// First, get the distance to this city
  		thisDistance = distance(
			position.coords.latitude, position.coords.longitude,
			thisCity[1], thisCity[2]);

  		// If this city is closer than the winner so far, we have a new winner
  		if (thisDistance<shortestDistanceSoFar) {
  			closestCitySoFar = thisCity;
        console.log('New closest city: '+thisCity[0]);
  			shortestDistanceSoFar = thisDistance;
        console.log('New distance to beat: '+thisDistance);
  		}

	}); //  next point
    console.log('Closest point: '+closestCitySoFar[0]);

    // now build the URL
    var theURL = $("#results").attr("base-local-url");
    theURL=theURL.replace('COUNTRYGOESHERE',closestCitySoFar[4]);
    theURL=theURL.replace('REGIONGOESHERE',closestCitySoFar[3]);
    theURL=theURL.replace('CITYGOESHERE',closestCitySoFar[5]);
    console.log('built URL: '+theURL);
    results.innerHTML = theURL;
    $("#results").attr("href", theURL);
}

