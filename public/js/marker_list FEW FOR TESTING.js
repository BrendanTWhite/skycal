var addressPoints = [
	['Valencia, Venezuela',10.1579312,-67.9972104,'None','Venezuela','Valencia'],
	['Burnie, Tasmania',-41.0528583,145.9052168,'Tasmania','Australia','Burnie'],
	['Butler, Pennsylvania',40.8611755,-79.8953328,'Pennsylvania','United_States','Butler'],
	['Peterborough, South Australia',-32.9742130,138.8343623,'South_Australia','Australia','Peterborough'],
	['Woomera, South Australia',-31.1656389,136.8192607,'South_Australia','Australia','Woomera'],
	['Adelaide, South Australia',-34.9284989,138.6007456,'South_Australia','Australia','Adelaide'],
	['Leigh Creek, South Australia',-30.5908141,138.3992865,'South_Australia','Australia','Leigh_Creek'],
	['Hanoi, Vietnam',21.0277644,105.8341598,'None','Vietnam','Hanoi']
];
