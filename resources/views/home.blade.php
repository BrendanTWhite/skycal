@extends('template')

@section('content')

<div>

  <div>Click the button to get your iCal link.</div>

  <button onclick="getLocation()">Go</button>

  <div>
    <a href="" id="results" base-local-url="{!! url('/') !!}/calendar/COUNTRYGOESHERE/REGIONGOESHERE/CITYGOESHERE.ics"></a>
  </div>

</div>

@endsection
